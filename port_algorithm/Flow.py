from Slots import *
class Flow:

    def __init__(self,demand_bw,flow_identity=0,size=0):
        self.size=size
        self.demand_bandwidth=demand_bw
        self.identity=flow_identity

     #get flows, demand_band_width
    def get_demand_BW(self):
        return self.demand_bandwidth

    #set demand bandwidth
    def set_demand_BW(self,demand_BW):
        self.demand_bandwidth=demand_BW

    def get_flowIdentity(self):
        return self.identity