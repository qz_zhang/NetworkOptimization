class Slots:
    per_slot_duration_time=0
    slot_num_with_port=0

    def __init__(self,duration,slots_num):
        self.per_slot_duration_time=duration
        self.slots_num=slots_num

    def set_duration(self,duration):
        self.per_slot_duration_time=duration

    def set_slots_num(self,slots):
        self.slot_num_with_port=slots
