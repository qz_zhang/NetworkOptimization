from __future__ import print_function
import copy
from ortools.linear_solver import pywraplp
import random
import numpy as np
from Flow import *
from Port import *

solver=pywraplp.Solver('Integer Programming',pywraplp.Solver.CBC_MIXED_INTEGER_PROGRAMMING)

def slot_assign():
    per_port_slot_num=10
    per_slot_bw=200

    flows=[]
    flows_path = {}
    slot_duration=Port.slot_attribute.per_slot_duration_time
    T=Port.slot_attribute.per_slot_duration_time*Port.slot_attribute.slot_num_with_port

    port_1 = Port('port1')
    port_2 = Port('port2')
    port_3 = Port('port3')
    port_4 = Port('port4')
    port_5 = Port('port5')
    port_6 = Port('port6')
    port_7 = Port('port7')

    for i in range(100000):
        demand = random.randint(20, 40)
        flow = Flow(demand)
        flow.identity='flow-'+str(i)
        flows.append(flow)

    #per port.identity should unique
    for i in range(len(flows)):
        #path as a port list
        flows_path[flows[i].identity]=[]
        rand_loop=random.randint(2,7)
        list_record=[]
        for j in range(rand_loop):
            rand_num=random.randint(1,7)
            if rand_num not in list_record:
                list_record.append(rand_num)
            else:
                continue
        # print('------------------------------')
        # print(list_record)


        #set ports that one flow will pass
        for l in range(len(list_record)):
            port_name = 'port_' + str(list_record[l])
            flows_path[flows[i].identity].append(eval(port_name))

        print(list_record)
        # print('------------------------------')


    # for i in range(len(flows)):
    #     for j in range(len(flows_path[flows[i].identity])):
    #         print(flows_path[flows[i].identity],end=' ')
    #     print()

    #set constraints
    #save flows.identity info
    key_flows_tuple = tuple(flows_path.keys())
    # print(key_flows_tuple)
    for i in range(len(flows_path)):
        #for every port in one path
        for j in range(len(flows_path[key_flows_tuple[i]])):
            temp_slots=[0 for _ in range(per_port_slot_num)]
            port_temp=flows_path[key_flows_tuple[i]][j]
            #print(temp_slots)
            #set slot variables for every slot in single port
            for k in range(per_port_slot_num):
                var_name='slot:'+key_flows_tuple[i]+'@'+flows_path[key_flows_tuple[i]][j].port_id
                temp_slots[k]=solver.IntVar(0.0,1.0,var_name)
            slot_identity=flows[i].identity+'@'+port_temp.port_id
            flows_path[key_flows_tuple[i]][j].slots_related2_flow[slot_identity]=temp_slots


    #save flows_info to port
    ports=[]
    #traverse all flows in flows_path dictionary
    for i in range(len(flows_path)):

        for j in range(len(flows_path[key_flows_tuple[i]])):
            #revise the attribute in port_temp will influence in ports in flows_path
            port_temp =flows_path[key_flows_tuple[i]][j]
            # print(port_temp.port_id,end=' ')
            slot_identity=flow.identity+'@'+port_temp.port_id
            if i == 0:
                port_temp.flows_2B_allocated.append(flows[i])
                ports.append(port_temp)
            elif i>=1:
                is_in_port=port_temp.is_one_port_in_ports(ports)

                if is_in_port[0]:
                    ports[is_in_port[1]].flows_2B_allocated.append(flows[i])
                else:
                    port_temp.flows_2B_allocated.append(flows[i])
                    ports.append(port_temp)
        # print()

    print('-------------------start len of =%d---------------------'%len(ports))
    print('--------------start print flows in port-----------')
    for i in range(len(ports)):
        print(ports[i].port_id,end=' ')
        keys=tuple(ports[i].slots_related2_flow.keys())
        print(keys)
        for j in range(len(ports[i].flows_2B_allocated)):
            print(ports[i].flows_2B_allocated[j].identity,end=' ')
        print()


    #set port constraints,one flow assign to single slot that of one port
    for i in range(len(ports)):
        #save single ports slot_keys related to flows
        slots_tuple=tuple(ports[i].slots_related2_flow.keys())
        # print(slots_tuple)
        # print(single_port_dic)
        for j in range(len(slots_tuple)):
            constraints=solver.Constraint(1,1)
            for k in range(len(ports[i].slots_related2_flow[slots_tuple[j]])):
                # print(single_port_dic[slots_tuple[j]][k])
                constraints.SetCoefficient(ports[i].slots_related2_flow[slots_tuple[j]][k],1)

    # set dealy constraints and dalay variables
    delay_variables = [[] for _ in range(len(flows))]
    delay_inherent = [[] for _ in range(len(flows_path))]
    key_f = tuple(flows_path.keys())
    for i in range(len(flows_path)):
        for j in range(len(flows_path[key_f[i]]) - 1):
            delay_r = random.randint(20, 80)
            delay_inherent[i].append(delay_r)
            dl_name = 'N_' +key_f[i]+'->'+ str(flows_path[key_f[i]][j].port_id + ',' + str(flows_path[key_f[i]][j + 1].port_id))
            delay_variables[i].append(solver.IntVar(0.0, solver.infinity(), dl_name))
            print('\033[1;35m %s \033[0m'%dl_name,end=' ')



    #constraints
    for i in range(len(flows_path)):
        deley_constraints=[]
        for j in range(len(flows_path[key_f[i]])-1):
            port_current=flows_path[key_f[i]][j]
            port_next=flows_path[key_f[i]][j+1]
            var_v_current= key_f[i] + '@' + port_current.port_id
            var_v_next =key_f[i]+'@'+port_next.port_id

            constraint_temp=solver.Constraint(delay_inherent[i][j], solver.infinity())
            constraint_temp.SetCoefficient(delay_variables[i][j],T)

            for k in range(len(port_current.slots_related2_flow[var_v_current])):
                constraint_temp.SetCoefficient(port_current.slots_related2_flow[ var_v_current][k],-k+1)
                constraint_temp.SetCoefficient(port_next .slots_related2_flow[var_v_next][k],k+1)
            deley_constraints.append(constraint_temp)

    objective=solver.Objective()

    for i in range(len(flows_path)):

        port_first = flows_path[key_f[i]][0]
        port_last = flows_path[key_f[i]][len(flows_path[key_f[i]])-1]
        var_v_first = key_f[i] + '@' + port_first.port_id
        var_v_last = key_f[i] + '@' + port_last.port_id
        for j in range(len(flows_path[key_f[i]]) - 1):

            objective.SetCoefficient(delay_variables[i][j],T)
            for k in range(len(port_current.slots_related2_flow[var_v_current])):
                objective.SetCoefficient(port_first.slots_related2_flow[var_v_first][k], -k + 1)
                objective.SetCoefficient(port_last.slots_related2_flow[var_v_last][k], k + 1)
    objective.SetMinimization()
    solver.Solve()

    print('Minimum objective function value = %d' % solver.Objective().Value())















# PER_SLOT_TOTAL_BW=100
# FLOWS_NUM=10
# SLOTS_NUM=20
# T=20
# INHERENT_DELAY=[]
# solver=pywraplp.Solver('IntegerProgramming',pywraplp.Solver.CBC_MIXED_INTEGER_PROGRAMMING)
#
# def AssignFlowsToQ(ppp):
#
#     #save port info ,each port info is a 2-dimension matrix
#     #ports is a three dimension
#     ports=copy.deepcopy(ppp)
#     delay = [[0 for _ in range(len(ports[0]))] for _ in range(len(ppp) - 1)]
#     variables = []
#
#     for i in range(len(ports)):
#         port_temp=ports[i]
#         set_port_constraints(port_temp)
#     set_delay_constraints(variables,ports,delay)
#
#     # 求解only one objective
#     objective=solver.Objective()
#     for i in range(len(ports)-1):
#         temp_port=ports[i]
#         last_port=ports[len(ports)-1]
#         #ports num
#         for j in range(len(temp_port)):
#             objective.SetCoefficient(variables[i][j], T)
#             #per slot in flow
#             # print('---------temp_port--------')
#             # print(temp_port)
#             #temp port is a dict type {key=flow_identity:value=slots[]}
#             key_tuple_temp=tuple(temp_port.keys())
#             key_tuple_last=tuple(ports[len(ports)-1].keys())
#             for k in range(len(key_tuple_temp[j])):
#                 # print('-------------k=%d----------'%k)
#                 objective.SetCoefficient(temp_port[key_tuple_temp[j]][k],-k)
#                 objective.SetCoefficient(last_port[key_tuple_last[j]][k],k)
#
#     objective.SetMinimization()
#     result_status=solver.Solve()
#     if result_status==pywraplp.Solver.OPTIMAL:
#         print('best')
#
#     print(type)
#     print('Min val: %d' % solver.Objective().Value())
#
#     #print all var values
#     for i in range(FLOWS_NUM):
#         for j in range(SLOTS_NUM):
#             print('%s = %d' % (variables[i][j].name(),variables[i][j].solution_value()),end='\t')
#             if j==SLOTS_NUM-1:
#                 print('\n')
#
#
#
# def set_port_constraints(single_port):
#     # this variables is dictionary type key is flow_identity value is the total slot list of one port
#     temp_port=single_port
#     key_tuple = tuple(temp_port.keys())
#     # print(key_tuple)
#
#     s='@'+key_tuple[0].split('@')[1]
#     # print(s)
#
#     num_flow=len(temp_port)
#     num_slots=len(temp_port[key_tuple[1]])
#
#     #set varibales
#     for i in range(num_flow):
#         for j in range(num_slots):
#             temp_port[key_tuple[i]][j]=solver.IntVar(0.0, 1.0, 'f_' + str(i) + ' -> ' + str(j)+s)
#
#     # per flow assgin to one slot
#     for i in range(num_flow):
#         constraint1 = solver.Constraint(1, 1)
#         for j in range(num_slots):
#             constraint1.SetCoefficient(temp_port[key_tuple[i]][j], 1)
#
#     #per slot bandwidth constraints
#     for i in range(num_slots):
#         constraint2 = solver.Constraint(0, PER_SLOT_TOTAL_BW)
#         for j in range(num_flow):                                                    #care
#             constraint2.SetCoefficient(temp_port[key_tuple[j]][i], random.randint(30, 40))
#
#
#
#
#     #
#     # for i in range(len(temp_port)):
#     #     print(result)
#     #     for j in range(num_slots):
#     #         print(temp_port[key_tuple[i]])
#     #     print()
#
#
# def set_delay_constraints(variables,ports,delay_constraints):
#     #
#     # print('--------print ports---------')
#     # print(ports)
#     # s = '@' + key_tuple[0].split('@')[1]
#     # print(s)
#     #every link 的固有时延
#     d=delay_constraints
#     flow_num = len(ports[0])
#     port_num= len(ports)
#     T=0
#
#     for i in range(port_num-1):
#         # print(variables)
#         temp=[]
#         for j in range(flow_num):
#             var_name='N'+str(i)+','+str(i+1)+'@f_'+str(j)
#             temp.append(solver.IntVar(0,solver.infinity(),var_name))
#         variables.append(temp)
#     # print('-----------variables-----------')
#     # print(variables)
#
#     for i in range(len(ports)-1):
#         temp_port=ports[i]
#         temp_port_2=ports[i+1]
#         key_tuple=tuple(temp_port.keys())
#         key_tuple_2=tuple(temp_port_2.keys())
#         # print('-------keys tuple---------')
#         # print(key_tuple)
#         for j in range(flow_num):
#             constraints=solver.Constraint(d[i][j],solver.infinity())
#             for k in range(len(temp_port[key_tuple[j]])):
#                 constraints.SetCoefficient(temp_port[key_tuple[j]][k],-k)
#                 constraints.SetCoefficient(temp_port_2[key_tuple_2[j]][k],k)
#                 print(variables[i][j])
#                 constraints.SetCoefficient(variables[i][j],T)
#
#


#define a main function
def main() :
    print()
    x = 3
    x = 'a'
    print(x)
    slot_assign()








    # port_1 = Port('port1')
    # port_2 = Port('port2')
    # port_3 = Port('port3')
    # port_4 = Port('port4')
    # port_5 = Port('port5')
    # port_6 = Port('port6')
    # dic={1:[port_1,port_2,port_4,port_6],
    #      2:[port_1,port_2,port_3],
    #      3:[port_1,port_2,port_3,port_4,port_5,port_6]}
    #
    # ports=[]
    # key_s=tuple(dic.keys())
    # print('len dic %d'%len(dic))
    # for i in range(len(dic)):
    #     # print('i =%d'%i)
    #     for j in range(len(dic[key_s[i]])):
    #         # temp_port=dic[key_s[i]][j]
    #         # print('%d,%d' % (i, j))
    #         if i==0:
    #             ports.append(dic[key_s[i]][j])
    #         else :
    #             if not (dic[key_s[i]][j].is_one_port_in_ports(ports)[0]):
    #                 print('%d,%d----' % (i, j))
    #                 print(dic[key_s[i]][j].is_one_port_in_ports(ports))
    #                 ports.append(dic[key_s[i]][j])
    #
    #
    # for i in range(len(ports)):
    #     print(ports[i].port_id,end=' ')
    #
    #
    #
    #
    #

    # flows = []
    # delay_constraints=[[]]
    # single_port_dic={}
    # single_port_dic2 = {}
    # single_port_dic3= {}
    # single_port_dic4= {}
    # single_port_dic5= {}
    # ppp=[]
    # slots_num_in_port=0
    # duration_per_slot=0
    # #set some flows
    #
    # flow_list=[]
    # for i in range(5):
    #     temp_flow=[]
    #     for j in range(10):
    #         demand=random.randint(20,40)
    #         flow=Flow(demand)
    #         temp_flow.append(flow)
    #     flow_list.append(temp_flow)
    #
    # port_1=Port('Port_1')
    # port_2=Port('Port_2')
    # port_3=Port('Port_3')
    # port_4=Port('Port_4')
    # port_5=Port('Port_5')
    #
    #
    # #if use '=' copy list the object is the list quote of source object
    # for i in range(len(flow_list)):
    #     print('---------len flow_list:%d--------'%len(flow_list))
    #     # port_i=locals()['port_'+str(i)]
    #     eval('port_'+str(i+1)).flows_2B_allocated=copy.deepcopy(flow_list[i])
    #     # print(port_i)
    #     # port_i
    #
    #
    # per_slot_duration = port_1.slot_attribute.per_slot_duration_time
    # slots_num_in_port = port_1.slot_attribute.slots_num
    #
    #
    # for i in range(10):
    #     port_1.flows_2B_allocated[i].identity='flow_'+str(i)+'@'+port_1.port_id
    #     port_2.flows_2B_allocated[i].identity='flow_' + str(i)+'@'+port_2.port_id
    #     port_3.flows_2B_allocated[i].identity = 'flow_' + str(i) + '@' + port_3.port_id
    #     port_4.flows_2B_allocated[i].identity = 'flow_' + str(i) + '@' + port_4.port_id
    #     port_5.flows_2B_allocated[i].identity = 'flow_' + str(i) + '@' + port_5.port_id
    #     # print(port_1.flows_2B_allocated[i].identity)
    #
    #
    # for i in range(10):
    #     single_port_dic[port_1.flows_2B_allocated[i].get_flowIdentity()]=[0]*slots_num_in_port
    #     single_port_dic2[port_2.flows_2B_allocated[i].get_flowIdentity()] = [0] * slots_num_in_port
    #     single_port_dic3[port_3.flows_2B_allocated[i].get_flowIdentity()] = [0] * slots_num_in_port
    #     single_port_dic4[port_4.flows_2B_allocated[i].get_flowIdentity()] = [0] * slots_num_in_port
    #     single_port_dic5[port_5.flows_2B_allocated[i].get_flowIdentity()] = [0] * slots_num_in_port
    #
    # ppp.append(single_port_dic)
    # ppp.append(single_port_dic2)
    # ppp.append(single_port_dic3)
    # ppp.append(single_port_dic4)
    # ppp.append(single_port_dic5)


    #when the program execute the loop,the flows of one port will be set over
    #every flow identity represent a list


if __name__=='__main__':
    main()

