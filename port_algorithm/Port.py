import time
from Flow import *
from Slots import *

class Port:
    slot_attribute=Slots(5,5)

    #key is the flow_id
    # slots_related2_flow={}
    def __init__(self,port_id,slots_related2_flow=None,flows_2B_allocated=None):

        self.port_id=port_id
        self.slots_related2_flow={}
        self.flows_2B_allocated=[]

    def __eq__(self, other):
        return self.port_id==other.port_id

    def get_port_id(self):
        return self.port_id

    def set_flows(self,flows):
        self.flows_2B_allocated=flows
        flows_in_port=len(flows)

    #return the total millisec from 1971-0-0 to now
    def get_current_millisec(self):
        return time.time()*1000

    def is_one_port_in_ports(self,ports):
        res=[0,0]
        isIn=False
        pos=-1

        for i in range(len(ports)):
            if self.port_id==ports[i].port_id:
                isIn=True
                pos=i
                break
                # print('res[0]=%s,res1=%d'%(res[0],res[1]))
                # print('%s VS %s '%(self.port_id,ports[i].port_id))
                # return res
        res[0]=isIn
        res[1]=pos
        return res



