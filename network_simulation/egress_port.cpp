#include "egress_port.h"

//non-parameter constructor set slot_num=10 <=> one egress_port has 10 VOQs
//per slot_duration is 5ms,cell_size_of_single_voq is 500b,one voq has 100 cells
EgressPort::EgressPort() {
	cout << "the EgressPort() constructor has been called!!!" << endl;
	this->egress_port_id_ = " ";
	this->slot_num_ = 10;
	this->slot_duration_time_ = 5; //5 millisecond,所以整个slot的周期为slot_num*slot_duration_time=50 ms
	this->cell_size_of_voq_ = 500;
	this->single_VOQ_size_ = 100;
	this->egress_voqs_.resize(this->slot_num_);
}

void EgressPort::set_slot_num(int slot_num) {
	this->slot_num_ = slot_num;
}

int EgressPort::get_slot_num() {
	return this->slot_num_;
}

void EgressPort::set_slot_duraton(int /*unit is millisecond*/duration) {
	this->slot_duration_time_ = duration;
}

int EgressPort::get_slot_duration() {
	return this->slot_duration_time_;
}

void EgressPort::set_single_VOQ_size(int voq_size) {
	this->single_VOQ_size_ = voq_size;
}

void EgressPort::set_cell_size(int cell_size) {
	this->cell_size_of_voq_ = cell_size;
}

int EgressPort::get_cell_size() {
	return this->cell_size_of_voq_;
}

void EgressPort::set_egress_id(string egress_id) {
	this->egress_port_id_ = egress_id;
}

string EgressPort::get_egress_id() {
	return this->egress_port_id_;
}

