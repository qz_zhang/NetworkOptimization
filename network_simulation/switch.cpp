#include "switch.h"


Switch::Switch(string switch_id,int egress_num, int node_id) {
	this->switch_id_ = switch_id;
	this->egerss_num_ = egress_num;
	this->node_in_network_ = node_id;
}

int Switch::get_node_id_in_internet() {
	return this->node_in_network_;
}

void Switch::set_node_id_in_internet(int node_id) {
	this->node_in_network_ = node_id;
}

void Switch::set_switch_id(string switch_id) {
	this->switch_id_ = switch_id;
}

string Switch::get_switch_id() {
	return this->switch_id_;
}


void Switch::set_egress_num(int out_num) {
	this->egerss_num_ = out_num;
}

int Switch::get_egress_num() {
	return  this->egerss_num_;
}

unordered_map<string, EgressPort> Switch::get_egress() {

	return this->egress_ports_;
}

void Switch::set_egress_port(int out_num) {

	
	//for each egress port,they have unique port id ,egress_port_id=switch_id+'_out_'+string(i)
	for (int i = 0; i<out_num; i++) {
		string port_id_in_current_switch = this->switch_id_ + "_out_" + to_string(i);
		EgressPort egress;
		egress.set_egress_id(port_id_in_current_switch);
		this->egress_ports_[port_id_in_current_switch] = egress;//再次调用拷贝构造函数
	}

	this->egerss_num_ = out_num;

}

void Switch::print_linksinfo() {
	//
	cout << "start print current switchs' links info!!!" << endl;
	int links_num = this->available_links_.size();
	for (int i = 0; i < links_num; i++) {
		cout << "link(" << available_links_[i].get_source_node() << ","
			<< available_links_[i].get_destination_node()<< ") "
			<< "distance: " << available_links_[i].get_distance()<< " km!" << endl;
	}
}

void Switch::push_into_cache(Packet p) {
	if (this->cache_queue_.size() < CACHE_QUEUE_MAX_SIZE) {
		this->cache_queue_.push(p);
	}
}

//
void Switch::move_into_egress_voq(int slot,int which_port) {
	if (which_port < 0 || which_port >= this->egerss_num_) {
		cout << "unlegal input!!!" << endl;
		return;
	}
	string egress_index = this->switch_id_ + "_out_" + to_string(which_port);
	if (slot < 0 || slot >= this->egress_ports_[egress_index].get_slot_num()) {
		cout << "slot input error!!!" << endl;
		return;
	}
	this->egress_ports_[egress_index].get_current_egress_voq()[slot].push(this->cache_queue_.front());
	this->cache_queue_.pop();

}

//return element number value of one VOQ of some Port 
int Switch::get_elemNum_inVoqOfsomePort(int slot, int which_port) {
	if (which_port < 0 || which_port >= this->egerss_num_) {
		cout << "unlegal input!!!" << endl;
		return -1;
	}
	string egress_index = this->switch_id_ + "_out_" + to_string(which_port);
	if (slot < 0 || slot >= this->egress_ports_[egress_index].get_slot_num()) {
		cout << "slot input error!!!" << endl;
		return -1;
	}

	return this->egress_ports_[egress_index].get_current_egress_voq()[slot].size();

}

queue<Packet>& Switch::get_voqOf_port(int which_slot, int which_port) {

	string egress_index = this->switch_id_ + "_out_" + to_string(which_port);
	return this->egress_ports_[egress_index].get_current_egress_voq()[which_slot];
	
}


void Switch::set_time_related_slot(long long update_time, int update_times) {

	if (update_times == 1) {
		for (int i = 0; i < PORT_NUM; i++) {
			vector<long long> single_slot;
			for (int j = 0; j < SLOT_N; j++) {
				single_slot.push_back(update_time + (long long)SLOT_D * i + (long long)SLOT_N * SLOT_D * j);
			}
			this->time_2_slot.push_back(single_slot);

		}
	}
	else {
		for (int i = 0; i < PORT_NUM; i++) {
			vector<long long> single_slot;
			for (int j = 0; j < SLOT_N; j++) {
				single_slot.push_back(update_time + (long long)SLOT_D * i + (long long)SLOT_N * SLOT_D * j);
			}
			this->time_2_slot[i] = single_slot;

		}
	}


}


void Switch::add_packet(ArrivalEvent e) {
	e.end_time = get_current_time();
	e.node_index++;
	e.pass_node = e.p->get_passby_nodes()[e.node_index];
	this->output_queue.push(e);

}

ArrivalEvent Switch::schedule_single_queue() {
	if (!output_queue.empty()) {
		
		ArrivalEvent ae = output_queue.top();
		long long ctime = get_current_time();
		long long timeAfterProcessing = ctime + (double)ae.packet_size / BAND_SLOT * 1000;
		ae.end_time = timeAfterProcessing;
		if (ae.pass_node == ae.final_node) {

			ae.p->set_total_delay(ae.end_time - ae.time);
			output_queue.pop();

			ArrivalEvent ae2 = output_queue.top();
			ae2.end_time = get_current_time() + (double)ae.packet_size / BAND_SLOT * 1000;
			return ae2;

		}
		else {
			output_queue.pop();
			return ae;
		}
	}


}


