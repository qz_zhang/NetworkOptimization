#include "network_link.h"

NetworkLink::NetworkLink() {
	this->source_node_ =-1;
	this->destination_node_ =-1;
	this->capacity_ = 200000000;
	this->distance_ = -1;
}

NetworkLink::NetworkLink(int source, int des, int dis,int capacity) {
	this->source_node_ = source;
	this->distance_ = dis;
	this->destination_node_ = des;
	this->capacity_ = capacity;
}