#pragma once
#include<string>
using namespace std;

class NetworkLink
{
public:

	NetworkLink();

	NetworkLink(int source, int des, int dis=0,int capacity=0);

	int get_source_node() {
		return this->source_node_;
	}

	int get_destination_node(){
		return this->destination_node_;
	}

	int get_distance() {
		return this->distance_;
	}

	bool operator==(NetworkLink& link) {
		if (this->source_node_ == link.source_node_ &&
			this->destination_node_ == link.destination_node_) {
			return true;
		}
		else {
			return false;
		}
	}


	int get_capacity() {
		return this->capacity_;
	}

private:
	int source_node_;
	int destination_node_;
	int capacity_;
	int distance_;
};

