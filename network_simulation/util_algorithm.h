#pragma once
#include"gurobi_c++.h"
#include"flow.h"
#include"network_link.h"
#include<vector>
#include"egress_port.h"
#include<stdlib.h>
#include<time.h>
#include<sys/timeb.h>
using namespace std;


int const SLOT_NUM = 10;
int const SLOT_DURATION = 5;
int const BANDWIDTH_PER_SLOT = 1000;


void swap_2_elem(int& a, int& b);

//return a sequence of randomint of range (0- range)
vector<int> generate_randomint_sequence(int range);

//get millisecond system time
long long get_current_time();



//suppose path key is flow_id ,path value is the vector of links associted with flow i
//size(link[flow.id])=size(map[flow.id])-1;

void assgin_packets2_slots(vector<Packet> packets);