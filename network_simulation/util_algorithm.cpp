#include"util_algorithm.h"
#include"gurobi_c++.h"
#include"flow.h"
#include"network_link.h"
#include<vector>
#include"egress_port.h"
#include<stdlib.h>
#include<time.h>
#include<sys/timeb.h>
#include<unordered_map>
using namespace std;





void swap_2_elem(int& a, int& b) {
	int tmep = a;
	a = b;
	b = tmep;
}

//return a sequence of randomint of range (0- range)
vector<int> generate_randomint_sequence(int range) {
	vector<int> result;
	result.clear();
	//srand(time(NULL));
	for (int i = 0; i < range; i++)
		result.push_back(i);

	while (--range)
	{
		int i = range;
		int j = rand() % range;
		swap_2_elem(result[i], result[j]);
	}

	/*for (int i = 0; i < range; i++)
		cout << result[i] << " ";
	cout << endl;*/
	return result;
}

//get millisecond system time
long long get_current_time() {
	long long time_last;
	time_last = time(NULL);

	struct timeb t1;
	ftime(&t1);
	time_t t = t1.millitm;//+ t1.time * 1000;
	//cout << t << endl;
	return t;
}



//suppose path key is flow_id ,path value is the vector of links associted with flow i
//size(link[flow.id])=size(map[flow.id])-1;

void assgin_packets2_slots(vector<Packet> packets) {
	try {
		//for single packet ,it will pass node: 1->2->0->3->4->5
		//for each pass-by node ,it will be assined to a fixed slot due to the delay on link.
		vector<vector<vector<GRBVar>>> assgined_slots;
		vector<vector<int>> inherent_delay;
		vector<vector<GRBVar>> n_delay;

		//key is node id,values is the packets is at current node.
		unordered_map<int, vector<Packet>> packets_at_nodes;


		//
		for (int i = 0; i < packets.size(); i++) {
			Packet current_p = packets[i];
			vector<int> passby_nodes = current_p.get_passby_nodes();
			int nodes_len = passby_nodes.size();
			for (int j = 0; j < nodes_len; j++) {
				packets_at_nodes[passby_nodes[j]].push_back(packets[i]);
			}


		}


		//save the propagation delay in packets
		for (int i = 0; i < packets.size(); i++) {
			inherent_delay.push_back(packets[i].get_inherent_delay());
		}


		GRBEnv env = GRBEnv(true);
		GRBModel model = GRBModel(env);

		//初始化slot分配变量
		for (int i = 0; i < packets.size(); i++) {
			Packet current_p = packets[i];
			vector<int> passby_nodes = current_p.get_passby_nodes();
			int nodes_num = passby_nodes.size();

			vector<vector<GRBVar>> nodes_slot;
			for (int j = 0; j < nodes_num; j++) {
				vector<GRBVar> slots;
				for (int k = 0; k < SLOT_NUM; k++) {
					slots.push_back(model.addVar(0.0, 1.0, 0.0, GRB_BINARY, "P_N_S_" + to_string(k)));
				}
				nodes_slot.push_back(slots);
			}
			assgined_slots.push_back(nodes_slot);
		}

		//初始化时延的n变量
		for (int i = 0; i < packets.size(); i++) {
			vector<int> dl = packets[i].get_inherent_delay();
			vector<GRBVar> dl_var;
			for (int j = 0; j < dl.size(); j++) {
				dl_var.push_back(model.addVar(0.0, GRB_INFINITY, 0.0, GRB_INTEGER, "d_n" + to_string(i) + "," + to_string(j)));
			}
			n_delay.push_back(dl_var);
		}

		//TODO:all var has been initialized,we need add constraints by using these variables
		//guranntee assgin to one slot
		for (int i = 0; i < packets.size(); i++) {
			Packet current_p = packets[i];
			vector<int> passby_nodes = current_p.get_passby_nodes();
			int nodes_num = passby_nodes.size();
			for (int j = 0; j < nodes_num; j++) {
				GRBLinExpr exp = 0;

				for (int k = 0; k < SLOT_NUM; k++) {
					exp = 1 * assgined_slots[i][j][k] + exp;
				}
				model.addConstr(exp, GRB_EQUAL, 1.0, "P" + to_string(i) + "@N" + to_string(j) + "to 1S");

			}

		}

		//packets be assgined to single slot should not exceed its bandwidth


	}
	catch (GRBException e) {

	}
}