#include "flow.h"
void Flow::set_packet(Packet packet) {	
	this->packets_in_flow_.push_back(packet);
}

int Flow::get_demand_bw(int statistical_time) {
	int count = 0;

	for (int i = 0; i <this->packet_num_; i++) {
		count = count + packets_in_flow_[i].get_packet_size();
	}

	return (int)(count / statistical_time);
}
