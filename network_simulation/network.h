#pragma once
#include"switch.h"
#include"network_link.h"


class Network
{
public:

	/*void init_network(vector<vector<int>> graph);

	void find_shortest_path(int source);*/

private:
	vector<Switch> switchs_;
	vector<NetworkLink> links_;
	vector<int> nodes_;  //save the node id of each switch in the whole network;
	vector<vector<int>> weights;
};

