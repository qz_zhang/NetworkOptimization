#pragma once
#include"packet.h"
#include<vector>
#include<queue>
#include<map>
#include"flow.h"
#include"gurobi_c++.h"

using namespace std;

class EgressPort {
public:
	EgressPort();

	void set_slot_num(int slot_num);

	int get_slot_num();

	void set_slot_duraton(int /*unit is millisecond*/duration);

	int get_slot_duration();

	void set_single_VOQ_size(int voq_size);

	void set_cell_size(int cell_size);

	int get_cell_size();

	void set_egress_id(string egress_id);

	string get_egress_id();

	vector<queue<Packet>>& get_current_egress_voq() {
		return this->egress_voqs_;
	}
	
	void set_egress_voqs(int slot_custom) {
		this->egress_voqs_.resize(slot_custom);
	}

private:
	string egress_port_id_;
	int slot_num_;
	int slot_duration_time_;

	//this attribute represent how many packets the VOQ can hold
	int single_VOQ_size_;

	int cell_size_of_voq_;
	//every egress port maintain slot_num's VOQ,single VOQ size is 20;
	//cell of single VOQ is 200kb
	vector<queue<Packet>> egress_voqs_;
	
};

