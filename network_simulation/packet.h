#pragma once
#include<vector>
using namespace std;
class Packet
{
public:
	Packet();

	Packet(int size, int id=-1);

	int get_packet_size();

	void set_packet_size(int size) {
		this->packet_size_ = size;
	}

	void update_arrival_time(int arrival);

	void update_departure_time(int departure);

	double get_total_delay();

	void set_current_time(double);

	void set_send_time(long long);

	double get_current_time() const{
		return this->current_time_;
	}

	void set_source_node(int source) {
		this->source_node_ = source;
	}

	void set_final_node(int final_node) {
		this->final_node_ = final_node;
	}

	void set_current(int current) {
		this->current_node_ = current;
	}

	void set_next_node(int next) {
		this->next_node_ = next;
	}

	vector<int>& get_passby_nodes() {
		return this->passby_nodes_;
	}

	int get_source() {
		return this->source_node_;
	}

	int get_final_node(){
		return this->final_node_;
	}

	int get_current_node() {
		return this->current_node_;
	}

	int get_next_node() {
		return this->next_node_;
	}

	vector<int>& get_assgied_slots() {
		return this->assgined_slot_;
	}

	void set_packet_id(int id) {
		this->packet_id_ = id;
	}

	bool operator==(Packet p) {
		return this->packet_id_ == p.packet_id_;
	}

	vector<int>& get_inherent_delay() {
		return this->inherent_delay_;
	}
	/*
	TODO:实现packet 根据自动路径走完网络
	*/
	void set_total_delay(int delay) {
		this->total_delay = delay;
	}

	friend bool operator<(Packet p1,Packet p2) {
		return p1.send_time_ > p2.send_time_;
	}

	void set_index(int index) {
		this->index = index;
	}

	int get_index() {
		return index;
	}

	int get_p_id() {
		return this->packet_id_;
	}

	vector<int>& get_path_length() {
		return this->path_length_;
	}

private:
	int packet_size_;
	int packet_id_;
	int arrival_ingress_time_;
	int departure_egress_time_;
	long long send_time_;//ms为单位
	double current_time_;
	double total_delay;

	int source_node_;
	int final_node_;
	int current_node_;
	int next_node_;
	int index;
	//this var saves the packet will passby_all nodes(source->final)
	vector<int> passby_nodes_;
	vector<int> assgined_slot_;
	vector<int> inherent_delay_; //unit is nanosecond
	vector<int>	path_length_;
};

