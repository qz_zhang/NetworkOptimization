#pragma once
#include<queue>
#include<vector>
#include"packet.h"
using namespace std;

class Ports
{
public:
	Ports();
	void set_port_id(int);
	int get_port_id();

private:
	int ports_id_;
	int ports_num_;
	int slot_num_;
	int slot_duration_time_;
	vector<queue<Packet>> v;

	
};

