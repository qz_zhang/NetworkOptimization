#pragma once
#include"packet.h"
#include<vector>
#include<string>
using namespace std;

class Flow
{
public:

	Flow() {
		packet_num_ = -1;
		demand_bw_ = -1;
		flow_id_ = " ";
	}

	void set_packet(Packet packet);

	int get_demand_bw(int statistical_time);

	int get_packet_num() {
		return packets_in_flow_.size();

	}

	string get_flow_id() {
		return this->flow_id_;
	}

private:
	int packet_num_;
	int demand_bw_;
	string flow_id_;
	vector<Packet> packets_in_flow_;

};

