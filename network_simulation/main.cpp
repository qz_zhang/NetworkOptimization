#include<iostream>
#include<time.h>
#include<string>
#include<vector>
#include"switch.h"
#include"flow.h"
#include"packet.h"
#include"network_link.h"
#include"util_algorithm.h"
#include<unordered_map>
#include<Windows.h>
#include <thread>


using namespace std;

const int M = INT_MAX;
const int SPEED_OF_LIGHT = 3e8;
const int BAND_OUT = 1e6; //1Mbps/per slot

struct datacmp {
	double data;
	int index;
	
	datacmp(int index, double data) {
		this->index = index;
		this->data = data;
	}
};

//void packet_flows(Packet packet, unordered_map<int, Switch> switchsinfo);

void generate_AbunchOf_packets(int generate_num,vector<Packet>& packets) {
	for (int i = 0; i < generate_num; i++) {
		Packet packet;

		//path save the packet 流经的节点值
		vector<int> path;
		vector<int> path_lens;

		//srand(1);
		int path_len = rand() % 4 + 2;
		int packet_size = rand() % 500 + 300; //500~800 b/per packet

		//set assigned slots all 0;
		vector<int> slots(path_len, 0);
		path.clear();
		path = generate_randomint_sequence(path_len);
		for (int i = 0; i < path.size(); i++)
			cout << path[i]<<" ";
		cout << endl;

		//packet保存它所经过的路径的长度，经过的链路数应为path数减去一
		path_lens.clear();
		for (int i = 0; i < path.size() - 1; i++) {
			int temp_len = rand() % 20 + 180;
			path_lens.push_back(temp_len);
			cout << "len " << temp_len << " ";
		}
		cout << endl;

		packet.get_path_length() = path_lens;
		packet.set_index(1);

		packet.get_passby_nodes() = path;
		packet.set_final_node(path[path.size() - 1]);
		packet.set_current(path[0]);
		packet.set_next_node(path[1]);
		packet.get_assgied_slots() = slots;
		packet.set_packet_size(packet_size);
		packet.set_packet_id(i);
		packet.set_send_time( get_current_time());
		//packets save the result of generated 包
		packets.push_back(packet);
	}
}


void generate_network_setSwitch(vector<vector<int>>& graph,unordered_map<int, Switch>& switchsinfo) {
	int a[] = { 0,4,M,2,M };
	int b[] = { 4,0,4,1,M };
	int c[] = { M,4,0,1,3 };
	int d[] = { 2,1,1,0,7 };
	int e[] = { M,M,3,7,0 };

	vector<int> temp1(a, a + sizeof(a) / sizeof(int));
	vector<int> temp2(b, b + sizeof(b) / sizeof(int));
	vector<int> temp3(c, c + sizeof(c) / sizeof(int));
	vector<int> temp4(d, d + sizeof(d) / sizeof(int));
	vector<int> temp5(e, e + sizeof(e) / sizeof(int));
	graph.push_back(temp1);
	graph.push_back(temp2);
	graph.push_back(temp3);
	graph.push_back(temp4);
	graph.push_back(temp5);

	//init switch
	Switch s1, s2, s3, s4, s5;
	s1.set_switch_id("switch0");
	s2.set_switch_id("switch1");
	s3.set_switch_id("switch2");
	s4.set_switch_id("switch3");
	s5.set_switch_id("switch4");

	s1.set_egress_port(1);
	s2.set_egress_port(1);
	s3.set_egress_port(1);
	s4.set_egress_port(1);
	s5.set_egress_port(1);

	s1.set_node_id_in_internet(0);
	s2.set_node_id_in_internet(1);
	s3.set_node_id_in_internet(2);
	s4.set_node_id_in_internet(3);
	s5.set_node_id_in_internet(4);

	switchsinfo[s1.get_node_id_in_internet()] = s1;
	switchsinfo[s2.get_node_id_in_internet()] = s2;
	switchsinfo[s3.get_node_id_in_internet()] = s3;
	switchsinfo[s4.get_node_id_in_internet()] = s4;
	switchsinfo[s5.get_node_id_in_internet()] = s5;

}
//queue simulate 
void Simulation() {
	/*construct a network graph*/
	vector<vector<int>> graph;
	unordered_map<int, Switch> switchsinfo;
	generate_network_setSwitch(graph, switchsinfo);
	

	priority_queue<ArrivalEvent> pq;
	vector<Packet> packets;

	generate_AbunchOf_packets(3, packets);

	for (int i = 0; i < packets.size(); i++) {
		ArrivalEvent ae;
		ae.p = &packets[i];
		ae.time = get_current_time();
		ae.packet_size = packets[i].get_packet_size();

		ae.node_index = 0;

		ae.pass_node = packets[i].get_passby_nodes()[ae.node_index];

		ae.end_time = 0;
		ae.final_node = ae.p->get_passby_nodes()[ae.p->get_passby_nodes().size() - 1];
		
		pq.push(ae);
	}

	while(!pq.empty()) {
		ArrivalEvent ae = pq.top();
		//cout << ae.time << " ";
		int switch_node_id = ae.pass_node;
		unordered_map<int, Switch>::iterator it = switchsinfo.find(switch_node_id);
		it->second.add_packet(ae);
		pq.push(it->second.schedule_single_queue());

		//cout << ae.pass_node << " ";
		pq.pop();
	}

	cout << "cout<<delay-------------------" << endl;
	for (int i = 0; i < packets.size(); i++)
		cout << "dedlay: " << packets[i].get_total_delay() << endl;

}

void switch_2do(Switch& s, unordered_map<int, Switch>& switchsinfo) {
	//switch process a packet
	
	if (!s.get_test_out().empty()) {
		Packet temp = s.get_test_out().top();
		double time_2b_update = 0.0;

		if (temp.get_final_node() != temp.get_current_node()) {
			//transmission delay
			time_2b_update = (double)temp.get_packet_size() / BAND_OUT*1e3;
			cout << "ID:" << temp.get_p_id() << " size is " << 
				temp.get_packet_size() << " byte " << " tansmission delay " << time_2b_update << endl;
			//cout << temp.get_packet_size() << endl;
			//计算packet link(i,j)上面的传输的时间
			//propagation delay
			cout << "temp.current node:" << temp.get_current_node() << endl;
			time_2b_update += (double)temp.get_path_length()[temp.get_index()-1] / SPEED_OF_LIGHT*10e3;
			
			cout <<"packet_ID "<<temp.get_p_id()<< " flows link(" << temp.get_current_node() 
				<< "," << temp.get_next_node() << ") length is "
				<< temp.get_path_length()[temp.get_index()-1];
			cout << "time_update " << time_2b_update << " " << endl;;

			temp.set_current(temp.get_next_node());
			temp.set_index(temp.get_index() + 1);

			if (temp.get_index() <= temp.get_passby_nodes().size() - 1) {
				temp.set_next_node(temp.get_passby_nodes()[temp.get_index()]);
			}

			temp.set_current_time(get_current_time() + time_2b_update);
			//代表packet进入下一个交换机
			switchsinfo.find(temp.get_current_node())->second.get_test_out().push(temp);
			s.get_test_out().pop();
		}
		else {
			//包到达最后的节点
			time_2b_update = (double)temp.get_packet_size() / BAND_OUT*1e3; //unit is millisecond
			cout << "ID " << temp.get_p_id() << "@ node " << temp.get_current_node() << " transmission delay "
				<< time_2b_update << endl;
			temp.set_current_time(get_current_time() + time_2b_update);
			std::cout << "packet " << temp.get_p_id() << " duration: " << temp.get_total_delay() << endl;
			s.get_test_out().pop();
		}

		
	}
	else {
		return;
	}
	
}

//将每个交换机队列的队首元素按照到达时间从小到大排列
void sorted_by_first_arrived(vector<datacmp> &arrived_t) {

	for (int i = 0; i < arrived_t.size(); i++) {
		for (int j = 0; j < arrived_t.size()-1-i; j++) {
			if (arrived_t[j].data > arrived_t[j + 1].data) {
				datacmp temp = arrived_t[j];
				arrived_t[j] = arrived_t[j + 1];
				arrived_t[j + 1] = temp;
			}
		}

	}
}

void Simulation2() {
	vector<vector<int>> graph;
	unordered_map<int, Switch> switchsinfo;
	generate_network_setSwitch(graph, switchsinfo);

	vector<Packet> test_packets;
	generate_AbunchOf_packets(3, test_packets);

	//set all generator packets to their first switch
	for (int i = 0; i < test_packets.size(); i++) {
		test_packets[i].set_current_time(get_current_time());
		switchsinfo.find(test_packets[i].get_current_node())->second.
			get_test_out().push(test_packets[i]);
	}


	while (1) {
		vector<datacmp> top_info;
		
		for (int i = 0; i < switchsinfo.size(); i++) {
			if (!switchsinfo[i].get_test_out().empty()) {
				datacmp record(i,
					switchsinfo[i].get_test_out().top().get_current_time());
				top_info.push_back(record);
			}
			
	   }
		if (!top_info.empty()) {
			sorted_by_first_arrived(top_info);
		}
		

		for (int i = 0; i < top_info.size(); i++) {
			switch_2do(switchsinfo[top_info[i].index],switchsinfo);
		}

		if (switchsinfo[0].get_test_out().empty() &&
			switchsinfo[1].get_test_out().empty() &&
			switchsinfo[2].get_test_out().empty() &&
			switchsinfo[3].get_test_out().empty() &&
			switchsinfo[4].get_test_out().empty()) {
			std::cout << "end loop" << endl;
			break;
		}
	}


	

}




void TimerTest() {
	UINT_PTR timer_id = SetTimer(NULL, 1, 1000, NULL);
	MSG msg;
	int i = 1;
	Switch s1;
	while (1) {
		GetMessage(&msg, NULL, 0, 0);
		if (msg.message == WM_TIMER) {
			//执行slot 更新
			
			s1.set_time_related_slot(get_current_time(), i);
			i++;
			s1.print_time_related_slot();
		}
		
	}

	

}


void switch_network_test() {
	/*construct a graph*/
	vector<vector<int>> graph;
	unordered_map<int, Switch> switchsinfo;

	int a[] = { 0,4,M,2,M };
	int b[] = { 4,0,4,1,M };
	int c[] = { M,4,0,1,3 };
	int d[] = { 2,1,1,0,7 };
	int e[] = { M,M,3,7,0 };

	vector<int> temp1(a, a + sizeof(a) / sizeof(int));
	vector<int> temp2(b, b + sizeof(b) / sizeof(int));
	vector<int> temp3(c, c + sizeof(c) / sizeof(int));
	vector<int> temp4(d, d + sizeof(d) / sizeof(int));
	vector<int> temp5(e, e + sizeof(e) / sizeof(int));
	graph.push_back(temp1);
	graph.push_back(temp2);
	graph.push_back(temp3);
	graph.push_back(temp4);
	graph.push_back(temp5);


	//init switch
	Switch s1,s2, s3, s4, s5;
	s1.set_switch_id("switch0");
	s2.set_switch_id("switch1");
	s3.set_switch_id("switch2");
	s4.set_switch_id("switch3");
	s5.set_switch_id("switch4");

	s1.set_egress_port(1);
	s2.set_egress_port(1);
	s3.set_egress_port(1);
	s4.set_egress_port(1);
	s5.set_egress_port(1);

	s1.set_node_id_in_internet(0);
	s2.set_node_id_in_internet(1);
	s3.set_node_id_in_internet(2);
	s4.set_node_id_in_internet(3);
	s5.set_node_id_in_internet(4);

	switchsinfo[s1.get_node_id_in_internet()] = s1;
	switchsinfo[s2.get_node_id_in_internet()] = s2;
	switchsinfo[s3.get_node_id_in_internet()] = s3;
	switchsinfo[s4.get_node_id_in_internet()] = s4;
	switchsinfo[s5.get_node_id_in_internet()] = s5;
	switchsinfo.find(1);

	int vertex_num = graph.size();
	for (int i = 0; i < vertex_num; i++) {
		unordered_map<int,Switch>::iterator temp_it = switchsinfo.find(i);
		
		vector<NetworkLink> links;
		for (int j = 0; j < graph[i].size(); j++) {

			if (graph[i][j] != 0 && graph[i][j] != M) {
				links.push_back(NetworkLink(i, j, graph[i][j]));
			}

			temp_it->second.set_available_links(links);
		}
	}

	for (int i = 0; i < 5; i++) {
		switchsinfo[i].print_linksinfo();
	}

	cout << "----------------------------packetzs test unit-------------------------------" << endl;
	//generate a bunch of packets
	vector<Packet> packets;
	packets.clear();
	//after generate a bunch of packets,we need to assgin these packets to initial switch(19:06 ,10/23/2019).
	generate_AbunchOf_packets(10, packets);
	cout << "generated packets size(): " << packets.size() << endl;

	//将生成的包移入switch缓冲区
	for (int i = 0; i < packets.size(); i++) {
		int source_switch = packets[i].get_passby_nodes()[0];
		cout << "ss :" << source_switch;
		unordered_map<int, Switch>::iterator it = switchsinfo.find(source_switch);
		//Switch& temp = it->second;
		it->second.push_into_cache(packets[i]);
	}
	cout << endl;


	for (int i = 0; i < switchsinfo.size(); i++)
		cout << "cs: " << switchsinfo[i].get_cache_queue().size()<<" ";
	cout << endl;
	cout << "cache size print over________________________________" << endl << endl;

	//刚开始将包从缓冲区移入对应的slot
	for (int i = 0; i < switchsinfo.size(); i++) {
		string voq_index = switchsinfo[i].get_switch_id() + "_out_0";
		cout << voq_index << " "  ;
		int cached_packet_size = switchsinfo[i].get_cache_queue().size();
		if (!switchsinfo[i].get_cache_queue().empty()) {
			for (int j = 0; j < cached_packet_size; j++) {

				Packet temp = switchsinfo[i].get_cache_queue().front();
				cout << "slot: " << temp.get_assgied_slots()[0];
				switchsinfo[i].move_into_egress_voq(temp.get_assgied_slots()[0], 0);
			}
			cout << endl;
		}
		else {
			//if current switch's cached queue is empty,traverse next switch.
			continue;
		}
			
	}

	cout << endl;
	
	for (int i = 0; i < switchsinfo.size(); i++) {
		cout << " "<<switchsinfo[i].get_elemNum_inVoqOfsomePort(0, 0)<<" "
		 << "cached queue size:" << switchsinfo[i].get_cache_queue().size();
	}
	cout << endl;
	
	//packet_flows(packet, switchsinfo);
	cout << "-------------------------test end---------------------------" << endl;
	cout << "flows end" << endl;

}

//simulate a packet flow through network
//TODO: simulate lots packet through network,packets save into switch voq ---update 10:14 2019/10/24
//
void packet_inSwitch_flow(unordered_map<int, Switch> switchsinfo) {
	int switch_num = switchsinfo.size();
	string switch_egressIndex = "";
	for (int i = 0; i < switch_num; i++) {
		
		

		
	}

	
}

//void packet_flows(Packet packet, unordered_map<int, Switch> switchsinfo) {
//	double t = 0.0;
//	double current_link_length = 0.0;
//	int every_packet_size = packet.get_packet_size();
//	vector<int> passby_nodes = packet.get_passby_nodes();
//	vector<int> assgined_slots = packet.get_assgied_slots();
//	int source = packet.get_source();
//	int final_node = packet.get_final_node();
//	int index = 1;
//	int current = packet.get_current_node();
//	int next = packet.get_next_node();
//	 
//	for (int i = 0; i < passby_nodes.size();i++) {
//	
//		if (i == 0) {///从第一个节点流出的逻辑
//			unordered_map<int, Switch>::iterator it = switchsinfo.find(current);
//			Switch temp = it->second;
//			vector<NetworkLink> avalilable_links = temp.get_avai_links();
//
//			NetworkLink link1(current, next);
//	
//			for (int j = 0; j < avalilable_links.size(); j++) {
//
//				if (avalilable_links[i].operator==(link1)) {//找到该走哪条链路
//					cout << "packet flows link(" << link1.get_source_node() << ","
//						<< link1.get_destination_node() << ")" << endl;
//					//transmission delay
//					current_link_length = avalilable_links[i].get_distance();
//					t += (double)every_packet_size / avalilable_links[i].get_capacity() + current_link_length / SPEED_OF_LIGHT;
//					break;
//				}
//			}
//			//节点流入下一个switch的缓冲区
//			current = next;
//			index++;
//			next = passby_nodes[index];
//
//		}
//		else {//从第一个节点流出后的逻辑
//			unordered_map<int, Switch>::iterator it = switchsinfo.find(current);
//			NetworkLink link(current, next);
//			vector<NetworkLink> avaliable_link = it->second.get_avai_links();
//			//将包送入缓冲区
//			it->second.get_cache_queue().push(packet);
//			if (it->second.get_cache_queue().front() == packet) {
//				it->second.move_into_egress_voq(0, 0);
//				if (it->second.get_voqOf_port(0, 0).front()== packet) {
//					//pan段当前时间是不是该slot的时间，如果是，发包，如果不是等待下一个周期到来
//					//如果包太大则选择下一个slot开始进行发包
//
//					if()
//					t +=(double)packet.get_packet_size() / link.get_capacity();
//				}
//				else {
//					//当前slot不是你要发的包
//				}
//			}
//
//
//		}
//		
//
//		
//
//	}
//}



int main() {
	/*for (int i = 0; i < 10; i++) {
		vector<int> v = generate_randomint_sequence(10);
		for (int j = 0; j < 10; j++)
			cout << v[j] << " ";
		cout << endl;
	}*/
	//switch_network_test();
	//srand(INFINITY);
	/*for (int i = 0; i < 10; i++)
		cout << rand() %5+2<< endl;*/

	/*vector<int> random = generate_randomint_sequence(10);
	for (int i = 0; i < random.size(); i++)
		cout << random[i] << " ";
	cout << endl;*/
	Simulation2();
	//get_current_time();

	//TimerTest();
	return 0;
}