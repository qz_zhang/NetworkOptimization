#include "packet.h"

Packet::Packet() {
	this->packet_id_ = -1;
	this->packet_size_ = -1; 
	this->departure_egress_time_ = -1;
	this->arrival_ingress_time_ = -1;
	this->send_time_ = -1;
	this->current_time_ = -1.0;
	this->assgined_slot_.clear();
	this->passby_nodes_.clear();
}


Packet::Packet(int size, int id) {
	this->packet_id_ = id;
	this->packet_size_ = size;
	this->departure_egress_time_ = -1;
	this->arrival_ingress_time_ = -1;
	this->send_time_ = -1;
	this->current_time_ = -1;
}

int Packet::get_packet_size() {
	return this->packet_size_;
}

void Packet::update_arrival_time(int arrival) {
	this->arrival_ingress_time_ = arrival;
}

void Packet::update_departure_time(int departure) {
	this->departure_egress_time_ = departure;
}

double Packet::get_total_delay() {
	return this->current_time_ - this->send_time_;
}

void Packet::set_current_time(double end) {
	this->current_time_ = end;
}

void Packet::set_send_time(long long send) {
	this->send_time_ = send;
}