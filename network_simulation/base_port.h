#pragma once
class BasePort
{
public:

	BasePort();
	
	void set_port_id(int);
	int  get_port_id();

private:
	int port_id_;
};

