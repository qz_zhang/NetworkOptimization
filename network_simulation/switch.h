#pragma once
#include<string>
#include<vector>
#include"egress_port.h"
#include<vector>
#include<unordered_map>
#include"network_link.h"
#include"util_algorithm.h"

using namespace std;

const int CACHE_QUEUE_MAX_SIZE = 100000;
const int SLOT_N = 10;
const int PORT_NUM = 10;
const int SLOT_D = 5;
const int BAND_SLOT = 1e6;//10的六次方

extern struct ArrivalEvent {
	Packet* p;
	long long time;
	int packet_size;
	int pass_node;
	int final_node;
	long long end_time;
	unsigned int node_index;
	
	friend bool operator<(ArrivalEvent ae1, ArrivalEvent ae2) {
		return ae1.end_time > ae2.end_time;
	}
};

class Switch
{
public:
	Switch() {
		this->egerss_num_ = -1;
		this->switch_id_ = " ";
		this->node_in_network_ = -1;
	}

	Switch(string switch_id, int egress_num, int node_id);

	~Switch() {

	};

	int get_node_id_in_internet();

	void set_node_id_in_internet(int);

	void set_switch_id(string);

	string get_switch_id();

	void set_egress_num(int);

	int get_egress_num();

	void set_egress_port(int);

	unordered_map<string, EgressPort>  get_egress();

	void print_linksinfo();

	void set_available_links(vector<NetworkLink> links) {
		this->available_links_ = links;
	}

	vector<NetworkLink>& get_avai_links() {
		return this->available_links_;
	}

	void push_into_cache(Packet p);
	
	//the slot value is 0,1,2,3,4,5,6,7,8,9 by default
	//which_port value is >=0&&<=egress_num-1
	void move_into_egress_voq(int slot_num,int which_port);

	//return element number value of one VOQ of some Port 
	int get_elemNum_inVoqOfsomePort(int slot, int which_port);

	queue<Packet>& get_cache_queue() {
		return this->cache_queue_;
	}

	queue<Packet>& get_voqOf_port(int which_slot, int which_port);
	
	void set_time_related_slot(long long,int);

	void print_time_related_slot() {
		for (int i = 0; i < time_2_slot.size(); i++) {
			for (int j = 0; j < time_2_slot[i].size(); j++) {
				cout << time_2_slot[i][j] << " ";
			}
			cout << endl;
		}
	}
	
	vector<vector<long long>>& get_time_2_slot() {
		return this->time_2_slot;
	}

	priority_queue<ArrivalEvent>& get_output_queue() {
		return this->output_queue;
	}

	void add_packet(ArrivalEvent e);

	ArrivalEvent schedule_single_queue();

	priority_queue<Packet>& get_test_out() {
		return this->test_output;
	}

private:
	//ingress like a link
	string switch_id_;

	int egerss_num_;
	int node_in_network_; //表示switch在整个网络中的标号
	//unordered_map is a set of ingress ports or egress port
	unordered_map<string,EgressPort> egress_ports_;
	vector<NetworkLink> available_links_;
	queue<Packet> cache_queue_;
	//switch应该有一个记录slot的时钟
	vector<vector<long long>> time_2_slot;
	priority_queue<ArrivalEvent> output_queue;
	priority_queue<Packet> test_output;

};





