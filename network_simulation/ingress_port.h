#pragma once
#include<queue>
#include<vector>
#include"packet.h"
#include"base_port.h"
using namespace std;

class IngressPort:public BasePort
{
public:
	
	void set_port_id(int);

	int get_port_id();

private:
	int current_packet_num_;
	vector<Packet> packets_in_ingress_;
	
};

